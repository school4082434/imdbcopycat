from json import dump, dumps, load, loads

f = open('bills-retrieved.json')

bill_data = load(f)

for bill in bill_data:
    bill_id = str(bill['bill']['type'] + bill['bill']['number'])
    bill_type = bill['bill']['type']
    bill_number = bill['bill']['number']
    bill_title = bill['bill']['title']
    policy_area = bill['bill']['policyArea']['name']

#     committees = 
#     sponsors = 
#     cosponsors = 

# for bill in bill_data['bill_array']:
#     bill_id = bill['bill_id']
#     bill_type = bill['bill_type']
#     bill_number = bill['bill_number']
#     bill_title = bill['bill_title']
#     policy_area = bill['policy_area']
#     committees = bill['committees']
#     sponsors = bill['sponsors']
#     cosponsors = bill['cosponsors']
