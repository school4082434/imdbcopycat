#!/usr/bin/env python3
"""
    this file is a modified verion ot the library of congress' CDG Examples
    the original version is copyrighted by the library of congress and
    released under a CC0 1.0 license
"""
from cdg_client import CDGClient
from json import dump, dumps, load, loads
import time
import itertools
import re

BILL_HR = "hr"
BILL_NUM = 21
BILL_PATH = "bill"
CONGRESS = 117
COMMITTEE_PATH = "committee"

def get_117th_bills(client):
    bills = []
    offset = 0
    endpoint = f"{BILL_PATH}/{CONGRESS}?offset={offset}&limit=250"

    start_time = time.time()
    data, status_code = client.get(endpoint)
    bills.append(data["bills"])

    total = data["pagination"]["count"]

    while True:
        try:
            data, status_code = client.get(data["pagination"]["next"])
            bills.append(data["bills"])
            #bills.append(data["bills"]["type"])
            #bills.append(data["bills"]["number"])
            print(round(len(bills)/.72, 2))

        except KeyError:
            print('--- finished in %.2f seconds ---' % (time.time() - start_time))
            with open('117th-bills.json', 'w') as f:
                dump(bills, f)
                break

def get_bill_info(client):
    with open('bill-ids.txt') as f:
        lines = f.readlines()

    bill_info = []

    for count, line in enumerate(lines):
        result = re.findall(r'(\w+?)(\d+)', line)[0]

        bill_info.append(get_bill_detail_helper(client, result[0], result[1]))
        print(round(count/178.16, 2))

    with open('117th-bill-info.json', 'w') as f:
        dump(bill_info, f)

    return 0

def get_member_info(client):
    """
    member info

    endpoints = [f"member/{member_id}",
                    f"member/{member_id}/sponsored-legislation",
                    f"member/{member_id}/cosponsored-legislation"]

    data, status_code = client.get(endpoint)
    """
    start_time = time.time()
    member_info = []

    with open('cmte-members-only.txt') as f:
        lines = f.readlines()

    for count, member_id in enumerate(lines):
        # member_info.append(member_data_helper(member_id))
        data, status_code = client.get(f"member/{member_id}/sponsored-legislation")
        member_info.append(data)
        print(round(count/5.25, 2))
        time.sleep(0.1)

    with open('117th-member-sponsored-leg.json', 'w') as f:
        dump(member_info, f)

    return print('--- finished in %.2f seconds ---' % (time.time() - start_time))


def get_bill_detail_helper(client, bill_type, bill_num):
    """
    'https://api.congress.gov/v3/bill/117/hr/21'
    This API returns list of all Bill details
    Bill Details
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{bill_type}/{BILL_NUM}"
    data, status_code = client.get(endpoint)
    return data


def get_bill_committee(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/committees'
    This API returns Committees of the specified Bill
    Bill Committees
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/committees"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_cosponsors(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/cosponsors'
    This API returns cosponsors of the specified Bill
    Bill Cosponsors
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/cosponsors"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_relatedbills(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/relatedbills'
    This API returns relatedbills of the specified Bill
    Bill Relatedbills
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/relatedbills"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_subjects(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/subjects'
    This API returns relatedbills of the specified Bill
    Bill subjects
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/subjects"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_summaries(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/summaries'
    This API returns summaries of the specified Bill
    Bill subjects
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/summaries"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_text(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/text'
    This API returns text of the specified Bill
    Bill subjects
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/text"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


def get_bill_titles(client):
    """
    'https://api.congress.gov/v3/bill/117/hr/21/titles'
    This API returns titles of the specified Bill
    Bill titles
    """
    endpoint = f"{BILL_PATH}/{CONGRESS}/{BILL_HR}/{BILL_NUM}/titles"
    data, status_code = client.get(endpoint)
    return print(dumps(data))


if __name__ == "__main__":
    """
    to run the file command :
        python bill.py <optional api version v3/v4>
        Example - python bill.py v3 or python bill.py
    """
    from dotenv import load_dotenv
    import os
    load_dotenv()
    api_key = os.getenv('CONGRESS_KEY')

    # if you want to view data in json format, you can change the output type here:
    client = CDGClient(api_key, response_format="json")

    print(f"Contacting Congress.gov, at {client.base_url} ...")
    pause = lambda: input('\nPress Enter to continue…')

    try:
        get_member_info(client)
        # get_117th_bills(client)
        # process_bills()
        # get_bill_info(client)
        # get_committee(client)
        # pause()
        # get_bill(client)
        # pause()
        # get_bill_congress(client)
        # pause()
        # get_bill_list_type(client)
        # pause()
        # get_bill_detail(client)
        # pause()
        # get_bill_action(client)
        # pause()
        # get_bill_amendments(client)
        # pause()
        # get_bill_committee(client)
        # pause()
        # get_bill_cosponsors(client)
        # pause()
        # get_bill_relatedbills(client)
        # pause()
        # get_bill_subjects(client)
        # pause()
        # get_bill_summaries(client)
        # pause()
        # get_bill_text(client)
        # pause()
        # get_bill_titles(client)

    except OSError as err:
        print('Error:', err)
