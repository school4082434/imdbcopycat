from flask import Flask, request, redirect, json, jsonify, send_from_directory, make_response
from commits import getCommits
from dotenv import load_dotenv
import os
from models import app, db, Member, Committee, Bill

from flask_cors import CORS

load_dotenv()

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
PROJECT_ID = os.getenv('PROJECT_ID')

# app = Flask(__name__)
# app.app_context().push()
cors = CORS(app, origin='*')

# landing page
@app.route('/', methods=['GET'])
def index():
    return {}

# return all members of congress
@app.route('/members/')
def members():
    mems = db.session.query(Member).all()

    # print(jsonify([e.make_json() for e in mems]))
    return jsonify(x=[e.make_json() for e in mems])

# return a single member's page using their bioguide id
@app.route('/members/<string:mem_id>')
def member_page(mem_id):
    print(f'mem_id is {mem_id}')
    mem = db.session.query(Member).filter_by(member_id=mem_id).first()
    return mem.make_json()

# return all bills
@app.route('/bills/')
def bills():
    blls = db.session.query(Bill).all()
    return jsonify(x=[e.make_json() for e in blls])

# return a single bill's page using its id
@app.route('/bills/<string:bll_id>')
def bill_page(bll_id):
    print(f'bll_id is {bll_id}')
    bll = db.session.query(Bill).filter_by(bill_id=bll_id).first()
    return bll.make_json()

# return all committees
@app.route('/committees/')
def committees():
    cmtes = db.session.query(Committee).all()
    return jsonify(x=[e.make_json() for e in cmtes])

# return a single committee's page using its id
@app.route('/committees/<string:cmte_id>')
def committee_page(cmte_id):
    print(f'cmte_id is {cmte_id}')
    cmte = db.session.query(Committee).filter_by(committee_id=cmte_id).first()
    return cmte.make_json()


@app.route('/about/')
def about_page():
    with open('about-example.json') as file:
        response = json.loads(file.read())
    return response

# API REQUESTS
# return all members of congress
@app.route('/api/members')
@app.route('/api/members/')
def api_members():
    mems = db.session.query(Member)

    search = request.args.get('search')
    if search:
        mems = Member.query.filter(Member.name.ilike('%' + search + '%'))


    sort_param = request.args.get('sort')
    order_by = request.args.get('order_by')
    if sort_param == 'name':
        if order_by == 'desc':
            mems = mems.order_by(Member.name.desc())
        else:
            mems = mems.order_by(Member.name.asc())
    
    elif sort_param == 'party':
        if order_by == 'desc':
            mems = mems.order_by(Member.party.desc())
        else:
            mems = mems.order_by(Member.party.asc())

    elif sort_param == 'state':
        if order_by == 'desc':
            mems = mems.order_by(Member.state.desc())
        else:
            mems = mems.order_by(Member.state.asc())

    elif sort_param == 'chamber':
        if order_by == 'desc':
            mems = mems.order_by(Member.chamber.desc())
        else:
            mems = mems.order_by(Member.chamber.asc())

    mems = mems.all()
    mem_api_response = []
    for mem in mems:
        mem_api_response.append({column.name: getattr(mem, column.name) for column in mem.__table__.columns})
    
    return jsonify(x=mem_api_response)

# return a single member's page using their bioguide id
@app.route('/api/members/<string:mem_id>')
def api_single_member(mem_id):
    print(f'mem_id is {mem_id}')
    mem = db.session.query(Member).filter_by(member_id=mem_id).first()
    return make_response(mem.make_json(), 200)

# return all bills
@app.route('/api/bills')
@app.route('/api/bills/')
def api_bills():
    blls = db.session.query(Bill)

    search = request.args.get('search')
    if search:
        blls = Bill.query.filter(Bill.bill_title.ilike('%' + search + '%'))


    sort_param = request.args.get('sort')
    order_by = request.args.get('order_by')
    if sort_param == 'bill_title':
        if order_by == 'desc':
            blls = blls.order_by(Bill.bill_title.desc())
        else:
            blls = blls.order_by(Bill.bill_title.asc())
    
    elif sort_param == 'bill_type':
        if order_by == 'desc':
            blls = blls.order_by(Bill.bill_type.desc())
        else:
            blls = blls.order_by(Bill.bill_type.asc())

    elif sort_param == 'policy_area':
        if order_by == 'desc':
            blls = blls.order_by(Bill.policy_area.desc())
        else:
            blls = blls.order_by(Bill.policy_area.asc())


    blls = blls.all()
    bll_api_response = []
    for bll in blls:
        bll_api_response.append({column.name: getattr(bll, column.name) for column in bll.__table__.columns})
    
    return jsonify(x=bll_api_response)

# return a single bill's page using its id
@app.route('/api/bills/<string:bll_id>')
def api_single_bill(bll_id):
    print(f'bll_id is {bll_id}')
    bll = db.session.query(Bill).filter_by(bill_id=bll_id).first()
    return make_response(bll.make_json(), 200)

# return all committees
@app.route('/api/committees')
@app.route('/api/committees/')
def api_committees():
    cmtes = db.session.query(Committee)

    search = request.args.get('search')
    if search:
        cmtes = Committee.query.filter(Committee.name.ilike('%' + search + '%'))


    sort_param = request.args.get('sort')
    order_by = request.args.get('order_by')
    if sort_param == 'name':
        if order_by == 'desc':
            cmtes = cmtes.order_by(Committee.name.desc())
        else:
            cmtes = cmtes.order_by(Committee.name.asc())
    
    elif sort_param == 'committee_type':
        if order_by == 'desc':
            cmtes = cmtes.order_by(Committee.committee_type.desc())
        else:
            cmtes = cmtes.order_by(Committee.committee_type.asc())

    elif sort_param == 'chamber':
        if order_by == 'desc':
            cmtes = cmtes.order_by(Committee.chamber.desc())
        else:
            cmtes = cmtes.order_by(Committee.chamber.asc())

    cmtes = cmtes.all()
    cmte_api_response = []
    for cmte in cmtes:
        cmte_api_response.append({column.name: getattr(cmte, column.name) for column in cmte.__table__.columns})
    
    return jsonify(x=cmte_api_response)

# return a single committee's page using its id
@app.route('/api/committees/<string:cmte_id>')
def api_single_committee(cmte_id):
    print(f'cmte_id is {cmte_id}')
    cmte = db.session.query(Committee).filter_by(committee_id=cmte_id).first()
    return make_response(cmte.make_json(), 200)

if __name__ == '__main__':
    app.run(debug=True)

# # committee example
# @app.route('/committees/single_committee')
# def example_single_committee():
#     with open('committee-example.json') as file:
#         response = json.loads(file.read())
#     return response

# # bill example
# @app.route('/bills/single_bill')
# def example_single_bill():
#     with open('bill-example.json') as file:
#         response = json.loads(file.read())
#     return response

# # member example
# @app.route('/members/single_member')
# def example_single_member():
#     with open('member-example.json') as file:
#         response = json.loads(file.read())
#     return response