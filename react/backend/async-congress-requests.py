from json import dump, load
import asyncio
import aiohttp
import re
from dotenv import load_dotenv
import os
import time

load_dotenv()
api_key = os.getenv('CONGRESS_KEY')

async def async_bill_helper(bill_type, bill_num, session):
    # endpoint = f'https://api.congress.gov/v3/bill/117/{bill_type}/{bill_num}?format=json'
    # endpoint = f'https://api.congress.gov/v3/bill/117/{bill_type}/{bill_num}/cosponsors?format=json&offset=0&limit=250'
    endpoint = f'https://api.congress.gov/v3/bill/117/{bill_type}/{bill_num}/committees?format=json&limit=250'
    headers = {"x-api-key": api_key}
    
    async with session.get(endpoint, headers=headers) as response:
        data = await response.json()
        print(response.headers['x-ratelimit-remaining'])
        return data

async def main():


    async with aiohttp.ClientSession() as session:
        with open('bill-ids-in-use.txt') as f:
            lines = f.readlines()

        bill_info = []

        # for count, line in enumerate(lines):
        for line in lines:
            result = line.split()

            data = await async_bill_helper(result[0], result[1], session)

            bill_info.append(data)
            # print(round(count/178.16, 2))

        with open('117th-bill-cmtes.json', 'w') as f:
            dump(bill_info, f)





if __name__ == '__main__':
    asyncio.run(main())

