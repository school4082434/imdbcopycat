from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import composite
import os
from dotenv import load_dotenv

app = Flask(__name__)
app.app_context().push()

load_dotenv()

PGUSER = os.getenv('PGUSER')
PASSWORD = os.getenv('PASSWORD')
IP_ADDRESS = os.getenv('IP_ADDRESS')
DBNAME = os.getenv('DBNAME')

app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{PGUSER}:{PASSWORD}@{IP_ADDRESS}/{DBNAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# association table for the many-to-many relationship between bills and their sponsors
sponsor_bill_association = db.Table('sponsor_bill_association',
    db.Column('member_id', db.String, db.ForeignKey('member_table.member_id')),
    db.Column('bill_id', db.String, db.ForeignKey('bill_table.bill_id'))
    )

# association table for the many-to-many relationship between bills and their cosponsors
cosponsor_bill_association = db.Table('cosponsor_bill_association',
    db.Column('member_id', db.String, db.ForeignKey('member_table.member_id')),
    db.Column('bill_id', db.String, db.ForeignKey('bill_table.bill_id'))
    )

# association table for the many-to-many relationship between member and committees
member_committee_association = db.Table('member_committee_association',
    db.Column('member_id', db.String, db.ForeignKey('member_table.member_id')),
    db.Column('committee_id', db.String, db.ForeignKey('committee_table.committee_id'))
    )

# association table for the many-to-many relationship between bills and committees
bill_committee_association = db.Table('bill_committee_association',
    db.Column('bill_id', db.String, db.ForeignKey('bill_table.bill_id')),
    db.Column('committee_id', db.String, db.ForeignKey('committee_table.committee_id'))
    )

class Member(db.Model):
    __tablename__ = 'member_table'

    member_id = db.Column(db.String(7), primary_key=True)
    name = db.Column(db.String(40))
    party = db.Column(db.String(30))
    chamber = db.Column(db.String(25))
    district = db.Column(db.String(2), nullable=True)  # Null if they are a senator
    state = db.Column(db.String(2))
 
    committees = db.relationship('Committee', secondary=member_committee_association, backref='members_backref')
    bills_sponsored = db.relationship('Bill', secondary=sponsor_bill_association, backref='sponsors_backref')
    bills_cosponsored = db.relationship('Bill', secondary=cosponsor_bill_association, backref='cosponsors_backref')

    def make_json(self):
       return {
            'member_id': self.member_id,
            'name': self.name,
            'party': self.party,
            'chamber': self.chamber,
            'district': self.district,
            'state': self.state,
            'committees': [(cmte.committee_id, cmte.name) for cmte in self.committees],
            'bills_sponsored': [(spn.bill_id, spn.bill_title) for spn in self.bills_sponsored],
            'bills_cosponsored': [(csp.bill_id, csp.bill_title) for csp in self.bills_cosponsored]
        }

class Committee(db.Model):
    __tablename__ = 'committee_table'

    committee_id = db.Column(db.String(7), primary_key=True)
    chamber = db.Column(db.String(25))
    name = db.Column(db.String(100))
    committee_type = db.Column(db.String(10))  # select, standing, or other
    n_sub = db.Column(db.Integer)

    bills_assigned = db.relationship('Bill', secondary=bill_committee_association, backref='committee_assignments_backref')
    members = db.relationship('Member', secondary=member_committee_association, backref='committee_assignments_backref')

    def make_json(self):
        return {
            'committee_id': self.committee_id,
            'committee_type': self.committee_type,
            'chamber': self.chamber,
            'name': self.name,
            'n_sub': self.n_sub,
            'members': [(mem.member_id, mem.name) for mem in self.members],
            'bills_assigned': [(bll.bill_id, bll.bill_title) for bll in self.bills_assigned]
        }

class Bill(db.Model):
    __tablename__ = 'bill_table'

    bill_id = db.Column(db.String(12), primary_key=True)
    bill_type = db.Column(db.String) # hr, s, hjres, sjres, hconres, sconres, hres, or sres
    bill_number = db.Column(db.String)
    bill_title = db.Column(db.String(1700))
    policy_area = db.Column(db.String)
    
    committees = db.relationship('Committee', secondary=bill_committee_association, backref='bills_assigned_backref')
    sponsors = db.relationship('Member', secondary=sponsor_bill_association,backref='bills_sponsored_backref')
    cosponsors = db.relationship('Member', secondary=cosponsor_bill_association, backref='bills_cosponsored_backref')

    def make_json(self):
        return {
            'bill_id': self.bill_id,
            'bill_type': self.bill_type,
            'bill_number': self.bill_number,
            'bill_title': self.bill_title,
            'policy_area': self.policy_area,
            'committees': [(cmte.committee_id, cmte.name) for cmte in self.committees],
            'sponsors': [(spn.member_id, spn.name) for spn in self.sponsors],
            'cosponsors': [(csp.member_id, csp.name) for csp in self.cosponsors]
        }