from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import composite
import os
import json
from dotenv import load_dotenv

app = Flask(__name__)
app.app_context().push()

load_dotenv()

PGUSER = os.getenv('PGUSER')
PASSWORD = os.getenv('PASSWORD')
IP_ADDRESS = os.getenv('IP_ADDRESS')
DBNAME = os.getenv('DBNAME')

app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{PGUSER}:{PASSWORD}@{IP_ADDRESS}/{DBNAME}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

from models import app, db, Member, Committee, Bill


with app.app_context():
    db.create_all()

f = open('117th-member-info.json')
member_data = json.load(f)
member_set = set()
for mem in member_data:
    member_id = mem['member']['bioguideId']
    name = mem['member']['directOrderName']
    party = mem['member']['partyHistory'][-1]['partyAbbreviation']
    chamber = mem['member']['terms'][-1]['chamber']
    try:
        district = mem['member']['terms'][-1]['district']
    except:
        district = None
    state = mem['member']['terms'][-1]['stateCode']

    committees_li = []
    # for c in committees:
    #     committees_li.append(Committee(committee_id=c))

    bills_spon_list = []
    # for bs in bills_sponsored:
    #     bills_spon_list.append(Bill(bill_id=str(bs['type'])+str(bs['number'])))

    bills_cospon_list = []
    # for bc in bills_cosponsored:
    #     bills_cospon_list.append(Bill(bill_id=str(bc['type'])+str(bc['number'])))
    

    # print(member_id, name, party, chamber, district, state, committees, bills_sponsored, bills_cosponsored)

    new_mem = Member(member_id=member_id, name=name, party=party, 
                     chamber=chamber, district=district, state=state,
                     committees=committees_li, bills_sponsored=bills_spon_list, 
                     bills_cosponsored=bills_cospon_list)

    member_set.add(member_id)

    # with app.app_context():
    db.session.merge(new_mem)
    db.session.commit()

f.close()

f = open('cmtes-recieved.json')

committee_data = json.load(f)
for com in committee_data['committees']:
    committee_id = com['systemCode']
    chamber = com['chamber']
    name = com['name']
    committee_type = com['committeeTypeCode']
    members = com['members']

    # bills_assigned = com['bills_assigned']

    # bills_assigned_list = []
    # for ba in bills_assigned:
    #     bills_assigned_list.append(Bill(bill_id=str(ba['type'])+str(ba['number'])))

    members_li = []
    for m in members:
        members_li.append(Member(member_id=m))

    try:
        n_sub = len(com['subcommittees'])
    except:
        n_sub = 0

    new_com = Committee(committee_id=committee_id, chamber=chamber, name=name,
                        committee_type=committee_type,
                        # bills_assigned=bills_assigned_list,
                        members=members_li, n_sub=n_sub)

    db.session.merge(new_com)
    db.session.commit()

f.close()


f = open('bills-retrieved.json')
g = open('117th-bill-cosponsors.json')
h = open('117th-bill-cmtes.json')

bill_data = json.load(f)
bill_cospon = json.load(g)
bill_cmtes = json.load(h)

for bill in bill_data:
    bill_id = str(bill['bill']['type'] + bill['bill']['number'])
    bill_type = bill['bill']['type']
    bill_number = bill['bill']['number']
    bill_title = bill['bill']['title']
    try:
        policy_area = bill['bill']['policyArea']['name']
    except:
        policy_area = None
    sponsors = bill['bill']['sponsors']

    committees_li = []
    for bc in bill_cmtes:
        if bc['request']['billType'].upper() == bill_type and bc['request']['billNumber'] == bill_number:
            for c in bc['committees']:
                committees_li.append(Committee(committee_id=c['systemCode']))

    bills_cospon_li = []
    for bc in bill_cospon:
        if bc['request']['billType'].upper() == bill_type and bc['request']['billNumber'] == bill_number:
            for csp in bc['cosponsors']:
                if csp['bioguideId'] not in member_set:
                    continue
                else:
                    bills_cospon_li.append(Member(member_id=csp['bioguideId']))

    bills_spon_li = []
    for bs in sponsors:
        if bs['bioguideId'] not in member_set:
            continue
        else:
            bills_spon_li.append(Member(member_id=bs['bioguideId']))

    new_bill = Bill(bill_id=bill_id, bill_type=bill_type, bill_number=bill_number, bill_title=bill_title,
                       policy_area=policy_area, committees=committees_li, sponsors=bills_spon_li,
                       cosponsors=bills_cospon_li)

    db.session.merge(new_bill)
    db.session.commit()

f.close()