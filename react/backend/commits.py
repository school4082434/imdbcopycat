import requests

def getCommits(ACCESS_TOKEN, PROJECT_ID):
    r"""
    Gets the number of commits for all branches

    args:
        * ACCESS_TOKEN (str): Access token for individual (e.g., token code)
        * PROJECT_ID (str): Project to scan branches for

    returns:
        * authors(dict{author_name:num_commits}): Dictionary of authors and commits
    """
    # Dict for getting authors and number of commits
    # {author_name:num_commits}
    authors = {}

    # GitLab API endpoint to list all branches in the project
    branches_url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/repository/branches'

    # Headers for authentication
    headers = {'Authorization': f'Bearer {ACCESS_TOKEN}'}

    # Fetch all branches
    branchResponse = requests.get(branches_url, headers=headers)

    if branchResponse.status_code == 200:
        branches = branchResponse.json()

        # Iterate through each branch and fetch its commits
        for branch in branches:
            branch_name = branch['name']
            commits_url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/repository/commits?ref_name={branch_name}'
            
            # Fetch commits for the branch
            commitResponse = requests.get(commits_url, headers=headers)
            
            if commitResponse.status_code == 200:
                commits = commitResponse.json()
                for commit in commits:
                    name = commit['author_name']
                    if name in authors:
                        authors[name] += 1
                    else:
                        authors[name] = 1
            else:
                return {'Error':commitResponse.status_code}
        return authors
    else:
        return {'Error':branchResponse}