import unittest
from models import app, db, Member, Bill, Committee
import time

class DBTestCases(unittest.TestCase):

    # setup
    @classmethod
    def setUpClass(cls):
        db.create_all()
    
    # ---------
    # members
    # ---------
    
    def test_member1(self):
        mem = Member(member_id='P000399', name='John Painter', party='L',
                        chamber='house', district='52', state='TX')

        db.session.merge(mem)
        db.session.commit()

        r = db.session.query(Member).filter_by(member_id = 'P000399').one()
        self.assertEqual(str(r.name), 'John Painter')
        self.assertEqual(str(r.party), 'L')

        # db.session.query(Member).filter_by(member_id='P000399').delete()
        db.session.commit()

    def test_member2(self):
        mem = Member(member_id='J000397', name='James George Janos', party='I',
                        chamber='senate', state='MN',
                        committees=[Committee(committee_id='jsic00')],
                        bills_sponsored=[Bill(bill_id='S324'), Bill(bill_id='SCONRES80')],
                        bills_cosponsored=[Bill(bill_id='HJRES2314')])


        db.session.merge(mem)
        db.session.commit()

        r = db.session.query(Member).filter_by(member_id = 'J000397').one()
        # print(r.bills_sponsored)

        self.assertEqual(str(r.bills_sponsored[0].bill_id), str(Bill(bill_id='S324').bill_id))
        self.assertEqual(str(r.bills_sponsored[1].bill_id), str(Bill(bill_id='SCONRES80').bill_id))

        # db.session.query(Member).filter_by(member_id='J000397').delete()
        db.session.commit()


    def test_member3(self):
        mem = Member(member_id='S001394', name='Vermin Love Supreme', party='A',
                        chamber='house', district='11', state='NH',
                        committees=[Committee(committee_id='sspn00'),
                                    Committee(committee_id='jsdh00')],
                        bills_sponsored=[Bill(bill_id='S0'),
                                            Bill(bill_id='SCONRES8080')], 
                        bills_cosponsored=[Bill(bill_id='SJRES002')])

        db.session.merge(mem)
        db.session.commit()

        r = db.session.query(Member).filter_by(name = 'Vermin Love Supreme').one()
        self.assertEqual(str(r.committees[0].committee_id),
                                str(Committee(committee_id='sspn00').committee_id))
        self.assertEqual(str(r.committees[1].committee_id),
                                str(Committee(committee_id='jsdh00').committee_id))

        # db.session.query(Member).filter_by(member_id='S001394').delete()
        db.session.commit()


    # ---------
    # committees
    # ---------

    def test_committee1(self):
        cmte = Committee(committee_id='jskn00', chamber='joint', name='Bills',
                            committee_type='standing',
                            bills_assigned=[Bill(bill_id='HJRES00')],
                            members=[Member(member_id='Z001394')])

        db.session.merge(cmte)
        db.session.commit()

        r = db.session.query(Committee).filter_by(committee_id = 'jskn00').one()
        self.assertEqual(str(r.committee_id), 'jskn00')

        # db.session.query(Committee).filter_by(committee_id='jskn00').delete()
        db.session.commit()

    def test_committee2(self):
        cmte = Committee(committee_id='hsfs00', chamber='joint', name='Committees',
                            committee_type='standing',
                            bills_assigned=[Bill(bill_id='HCONRES82')],
                            members=[Member(member_id='E101394')])

        db.session.merge(cmte)
        db.session.commit()

        r = db.session.query(Committee).filter_by(committee_id = 'hsfs00').one()
        self.assertEqual(str(r.bills_assigned[0].bill_id), str(Bill(bill_id='HCONRES82').bill_id))

        # db.session.query(Committee).filter_by(committee_id='hsfs00').delete()
        db.session.commit()

    def test_committee3(self):
        cmte = Committee(committee_id='sspm00', chamber='joint', name='Standards',
                            committee_type='select',
                            bills_assigned=[Bill(bill_id='HJRES00')],
                            members=[Member(member_id='S001394')])

        db.session.merge(cmte)
        db.session.commit()

        r = db.session.query(Committee).filter_by(committee_id = 'sspm00').one()
        self.assertEqual(str(r.chamber), 'joint')
        self.assertEqual(str(r.committee_type), 'select')
        self.assertEqual(str(r.members[0].member_id), str([Member(member_id='S001394')][0].member_id))

        # db.session.query(Committee).filter_by(committee_id='sspm00').delete()
        db.session.commit()


    # ---------
    # bills
    # ---------

    def test_bill1(self):
        bill = Bill(bill_id='H203', bill_type='H',
                    bill_number='203', bill_title='A bill to',
                    policy_area='Congress',
                    committees=[Committee(committee_id='jskn02')],
                    sponsors=[Member(member_id='V103294')],
                    cosponsors=[Member(member_id='D942114')])

        db.session.merge(bill)
        db.session.commit()

        r = db.session.query(Bill).filter_by(bill_id = 'H203').one()
        self.assertEqual(str(r.bill_id), 'H203')

        # db.session.query(Bill).filter_by(bill_id='H203').delete()
        db.session.commit()

    def test_bill2(self):
        bill = Bill(bill_id='SJRES8293', bill_type='SJRES',
                    bill_number='8293', bill_title='A resolution to',
                    policy_area='Bureaucracy',
                    committees=[Committee(committee_id='sspm02')],
                    sponsors=[Member(member_id='W346895')],
                    cosponsors=[Member(member_id='E178203')])

        db.session.merge(bill)
        db.session.commit()

        r = db.session.query(Bill).filter_by(bill_id = 'SJRES8293').one()
        self.assertEqual(str(r.committees[0].committee_id),
                            str([Committee(committee_id='sspm02')][0].committee_id))
        self.assertEqual(str(r.policy_area), 'Bureaucracy')

        # db.session.query(Bill).filter_by(bill_id='SJRES8293').delete()
        db.session.commit()


    def test_bill3(self):
        bill = Bill(bill_id='H2103', bill_type='H',
                    bill_number='2103', bill_title='A bill to test',
                    policy_area='Congress',
                    committees=[Committee(committee_id='hsut00')],
                    

                    sponsors=[Member(member_id='H029482')],


                    cosponsors=[Member(member_id='N302859')])

        db.session.merge(bill)
        db.session.commit()


        r = db.session.query(Bill).filter_by(bill_id = 'H2103').one()

        # print('sponsors:', r.sponsors)
        # print('cosponsors:', r.cosponsors)

        self.assertEqual(str(r.sponsors[0].member_id),
                            str(Member(member_id='H029482').member_id))
        self.assertEqual(str(r.cosponsors[0].member_id),
                            str([Member(member_id='N302859')][0].member_id))

        # db.session.query(Bill).filter_by(bill_id='H2103').delete()
        db.session.commit()


    # teardown
    @classmethod
    def tearDownClass(cls):
        db.drop_all()


if __name__=='__main__':
    unittest.main()
    