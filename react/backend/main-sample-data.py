from flask import Flask, render_template, request, redirect, json, jsonify, send_from_directory
from commits import getCommits
from dotenv import load_dotenv
import os

from flask_cors import CORS

load_dotenv()

ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
PROJECT_ID = os.getenv('PROJECT_ID')

app = Flask(__name__)
cors = CORS(app, origin='*')

# landing page
@app.route('/', methods=['GET'])
def index():
    return {}

# return all members of congress
@app.route('/members/')
def members():
    with open('multiple-member-example.json') as file:
        response = json.loads(file.read())
    return response

# return a single member's page using their bioguide id
@app.route('/members/<string:member_id>')
def member_page(member_id):
    with open('member-example.json') as file:
        response = json.loads(file.read())
    return response

# return all bills
@app.route('/bills/')
def bills():
    with open('multiple-bill-example.json') as file:
        response = json.loads(file.read())
    return response

# return a single bill's page using its id
@app.route('/bills/<string:bill_id>')
def bill_page(bill_id):
    with open('bill-example.json') as file:
        response = json.loads(file.read())
    return response

# return all committees
@app.route('/committees/')
def committees():
    with open('multiple-committee-example.json') as file:
        response = json.loads(file.read())
    return response

# return a single committee's page using its id
@app.route('/committees/<string:committee_id>')
def committee_page(committee_id):
    with open('committee-example.json') as file:
        response = json.loads(file.read())
    return response

# committee example
@app.route('/committees/single_committee')
def example_single_committee():
    with open('committee-example.json') as file:
        response = json.loads(file.read())
    return response

# bill example
@app.route('/bills/single_bill')
def example_single_bill():
    with open('bill-example.json') as file:
        response = json.loads(file.read())
    return response

# member example
@app.route('/members/single_member')
def example_single_member():
    with open('member-example.json') as file:
        response = json.loads(file.read())
    return response

@app.route('/about/')
def about_page():
    with open('about-example.json') as file:
        response = json.loads(file.read())
    return response

if __name__ == '__main__':
    app.run(debug=True)

