import { Box } from '@chakra-ui/react'
import React from 'react'
import "./SearchResultsList.css"
import SearchResultMembers from './SearchResultPages/SearchResultMembers'

const SearchResultsList = ({results}) => {
  return (
    <Box className='results-list'>
        
        {
            results.map((results, id) => {
                // Change this
                return <SearchResultMembers result={results} key={id}/>
            })
        }
    </Box>
  )
}

export default SearchResultsList
