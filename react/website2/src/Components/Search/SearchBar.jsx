import React, {useState} from 'react'
import {FaSearch} from 'react-icons/fa'
import './SearchBar.css'

const SearchBar = ({data, setResults, allData}) => {
    //console.log(arrData)
    const [input, setInput] = useState("")

    const fetchData = (value) => {
      if (!value) {
          // If the search input is empty, revert to showing all data
          setResults(allData);
          console.log("Input is blank, reverting to allData");
      } else {
          const results = data.filter((user) => {
              // Check if any property value includes the given value
              return Object.values(user).some(propertyValue =>
                  typeof propertyValue === 'string' && 
                  propertyValue.toLowerCase().includes(value.toLowerCase())
              );
          });
  
          setResults(results);
      }
  }  

    const handleChange = (value) => {
        setInput(value)
        fetchData(value)
    }

  return (
    <div className='input-wrapper'>
      <FaSearch id='searchIcon'/>
      <input placeholder='Type to search...' 
      value={input} 
      onChange={(e) => handleChange(e.target.value)}/>
    </div>
  )
}

export default SearchBar
