import React from 'react'
import "../SearchResult.css"
import { Box } from '@chakra-ui/react'
import { Link } from 'react-router-dom'

const SearchResultMembers = ({result}) => {
    console.log(result)
  return (
    <Box className='search-result'>
      <Link to={`/members/${result.member_id}`}>{result.name}, {result.party}, {result.state}, {result.chamber}</Link>
    </Box>
  )
}

export default SearchResultMembers
