import React, { useEffect, useState } from 'react';
import './MemberDetails.css';
import { Link } from "react-router-dom"

const MemberDetails = ({props}) => {
  console.log(Object.keys(props))
  const img_src = "https://theunitedstates.io/images/congress/450x550/"+ props.member_id + ".jpg"
  
  const [committees, setCommittees] = useState(props.committes || [])
  useEffect(() => {
    setCommittees(props.committees)
  }, [props]) // Re-run the effect if prop changes  

  const [billsSponsored, setBillsSponsored] = useState(props.bills_sponsored || [])
  useEffect(() => {
    setBillsSponsored(props.bills_sponsored)
  }, [props]) // Re-run the effect if prop changes  

  const [billsCosponsored, setBillsCosponsored] = useState(props.bills_cosponsored || [])
  useEffect(() => {
    setBillsCosponsored(props.bills_cosponsored)
  }, [props]) // Re-run the effect if prop changes  

  return (
  <div style={{ textAlign: 'center' }}>
    <h1 className='customH1'>{props.name}</h1>
    <br/>
    <img src={img_src} alt="Member" style={{ maxWidth: '400px', maxHeight: '400px', margin: '0 auto' }} />
    <div>
      <br/>
      <p>
        <strong>Party Name:</strong> {props.party}
      </p>
      
      <p>
        <strong>District:</strong> {props.district}
      </p>
      
      <p>
        <strong>State:</strong> {props.state}
      </p>

      <p>
        <strong>Chamber:</strong> {props.chamber}
      </p>
      <br/>
      <div>
        <h2 className='customH2'>Committees</h2> 
        <ul className="memberHeader">
          {committees.map((committee, index) => (
            <li className="memberItem" key={index}>
              <Link to={`/committees/${committee[0]}`}>{committee[1]}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <h2 className='customH2'>Bills Sponsored</h2> 
        <ul className="memberHeader">
          {billsSponsored.map((billId, index) => (
            <li className="memberItem" key = {index}>
              <Link to={`/bills/${billId[0]}`}>{billId[1]}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <h2 className='customH2'>Bills Cosponsored</h2> 
        <ul className="memberHeader">
          {billsCosponsored.map((billId, index) =>(
            <li className="memberItem" key={index}>
              <Link to={`/bills/${billId[0]}`}>{billId[1]}</Link>
              <br/><br/>
            </li>
          ))}
        </ul>
      </div>

    </div>
  </div>
  );
};

export default MemberDetails;


