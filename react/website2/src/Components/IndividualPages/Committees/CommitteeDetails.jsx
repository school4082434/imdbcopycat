import { React, useState, useEffect } from 'react';
import './CommitteeDetails.css';
import { Link } from "react-router-dom";


const CommitteeDetails = ({ props }) => {
  // Destructure member object to access individual properties
  // const { name, membership, leadership, legislations, committee, chamber, type } = props;

  // Setting committees to update when props updates b/c async
  const [bills_assigned, setCommittees] = useState(props.bills_assigned || [])
  useEffect(() => {
    setCommittees(props.bills_assigned)
  }, [props]) // Re-run the effect if prop changes  
  //console.log(Object.keys(props))

  return (
    <div style={{ textAlign: 'center' }}>
      <h1 className='customH1'>{props.name}</h1>

      <br/>

      <p><strong>Chamber:</strong> {props.chamber}</p>
      <p><strong>Committee Type:</strong> {props.committee_type}</p>
        
      <br/>
        <h2 className='customH2'>Members</h2> 
        <ul className='committeeMembers'>
          {props.members && props.members.map((memberId, index) =>(
            <li className='committeeItem' key = {index}>
              <Link to={`/members/${memberId[0]}`}>{memberId[1]}</Link>
            </li>
          ))}
        </ul>

        <h2 className='customH2'>Bills Assigned</h2> 
        <ul className='committeeHeader'>
          {bills_assigned && bills_assigned.map((billId, index) =>(
            <li className='committeeItem' key={index}>
              <Link to={`/bills/${billId[0]}`}>{billId[1]}</Link>
            </li>
          ))}
        </ul>

    </div>
  );
};

export default CommitteeDetails;


