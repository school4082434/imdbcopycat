import { Button, Box} from "@chakra-ui/react"
import "./Card.css"
import { Link } from "react-router-dom"

// Props: {photo_id, name, party, state, chamber, memberid}
const Card = ({props}) => {
    const img_src = "https://theunitedstates.io/images/congress/450x550/"+ props.member_id + ".jpg"
    return (
        <Box className="memberCard">
            <img className = "cardImage" alt="name" src={img_src}/>
            <br/>
            <h2>{props.name}</h2>
            <p>Party: {props.party === "D" ? "Democrat" : props.party === "R" ? "Republican" : "Independent"}</p>
            <p>State: {props.state}</p>
            <p>{props.chamber}</p>
            <br/>
            <Button><Link to={`/members/${props.member_id}`}>Show More</Link></Button>
        </Box>
    )
}

export default Card