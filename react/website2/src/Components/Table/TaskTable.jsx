import { Box, Button, ButtonGroup, Icon, Text } from "@chakra-ui/react";
import { flexRender, getCoreRowModel, getPaginationRowModel, getSortedRowModel, useReactTable } from "@tanstack/react-table";
import { useEffect, useState } from "react";
import SortIcon from './icons/SortIcon'

const TaskTable = (props) => {
  const columns = props.columns
  const [data, setData] = useState(props.data)
  useEffect(() => {
    setData(props.data);
  }, [props.data]); // Re-run the effect if props.data changes  

  /* table parameters: data (JSON representing row),*/
  const table = useReactTable({
    data,
    columns,
    // Allows the get coremodel to work in the background
    getCoreRowModel: getCoreRowModel(),
    //TODO getFilteredRowModel: getFilteredRowModel()
    getSortedRowModel: getSortedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    ColumnResizeMode: "onChange",
  });
  // Mapping through header groups since information is very nested
  // We need to use flexRender to render data in our table if the data is a react 
  // component like props (see cell)
  return (
    <Box>
      <Box className="table" w={table.getTotalSize()} style={{ marginLeft: '20px' }}>
        {table.getHeaderGroups().map((headerGroup) => (
          <Box className="tr" key={headerGroup.id}>
            {headerGroup.headers.map((header) => (
              <Box className="th" key={header.id} w={header.getSize()}>
                {header.column.columnDef.header}
                {header.column.getCanSort() && (
                  <Icon
                    as={SortIcon}
                    mx={3}
                    fontSize={14}
                    onClick={header.column.getToggleSortingHandler()}
                  />
                )}
                {
                  {
                    asc: "⬆️",
                    desc: "⬇️",
                  }[header.column.getIsSorted()]
                  
                }
                <Box
                  onMouseDown={header.getResizeHandler()}
                  onTouchStart={header.getResizeHandler()}
                  className={`resizer ${
                      header.column.getIsResizing() ? "isResizing" : ""
                  }`}
                />
              </Box>
            ))}
          </Box>
        ))}
        {table.getRowModel().rows.map((row) => (
          <Box className="tr" key={row.id}>
            {row.getVisibleCells().map((cell) => (
              <Box className="td" key={cell.id} w={cell.column.getSize()}>
                {flexRender(cell.column.columnDef.cell, cell.getContext())}
              </Box>
            ))}
          </Box>
        ))}
      </Box>
      <br/>
      <Text mb={2} style={{ marginLeft: '20px' }}>
        Page {table.getState().pagination.pageIndex+1} of {table.getPageCount()}
      </Text>
      <ButtonGroup size='sm' isAttached variant={'outline'} style={{ marginLeft: '13px' }} padding='4px'>
        <Button onClick={() => table.previousPage()} isDisabled={!table.getCanPreviousPage()}> {"<"} </Button>
        <Button onClick={() => table.nextPage()} isDisabled={!table.getCanNextPage()}> {">"} </Button>
      </ButtonGroup>
    </Box>
    );
  };

export default TaskTable;
