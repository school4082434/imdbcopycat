import Navbar from "../NavBar/Navbar";
import CommitteeDetails from "../IndividualPages/Committees/CommitteeDetails"
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const Committee = () => {
  const [data, setData] = useState([]);
  const { committee_id } = useParams();
  console.log("Committee ID:", committee_id);

  console.log(committee_id)
  useEffect(() => {
    Utils.GetJSON(`/committees/${committee_id}`).then(resolvedData => {
      setData(resolvedData);
    });
  }, [committee_id]); // React to changes in committeeId

  return (
    <div>
      <Navbar/>
      <CommitteeDetails props={data} />
    </div>
  );
};

export default Committee;
