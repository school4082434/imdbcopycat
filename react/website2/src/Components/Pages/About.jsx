import { Box } from "@chakra-ui/react";
import Navbar from "../NavBar/Navbar";
import { useEffect, useState } from "react";
import Utils from "../Utils";
import AboutCard from "../AboutCard/AboutCard";
import UnitTests from "../../assets/unit-tests.png"
import UML from "../../assets/uml_diagram.png"

const About = () => {
    const [data, setData] = useState([])

    useEffect(() => {
        Utils.GetJSON("/about/").then(resolvedData => {
            setData(resolvedData);
        });
    }, []);

    return(
        <Box>
            <Navbar/>
            <br/>
            {Object.entries(data).map(([person, commits], index) =>(
                <AboutCard name={person} commits={commits}/>
            ))}
            <br/> <br/> 
            <h1>Statistics</h1>
            <ul className="committeeHeader">
                <li className="committeeItem">Total commits: 393</li>
                <li className="committeeItem">Total issues: 22</li>
                <li className="committeeItem">Total number of unit tests: 9</li>
                <li> 
                    <a href="https://documenter.getpostman.com/view/33692704/2sA3Bj9ufg" target="_blank" rel="noopener noreferrer">
                        Postman documentation
                    </a>
                </li>
                <li>
                    <a href="https://dark-shadow-760426.postman.co/workspace/Team-Workspace~412ef79f-ee31-4779-8c26-d0147dd0b262/collection/33692704-db079405-4960-479d-9f8e-433a4974a8e1?action=share&creator=33692704&active-environment=33692704-4644aea9-6b93-4565-872d-45ab59688821">
                        Postman workspace with tests
                    </a>
                </li>
                <li>
                    <a href="https://gitlab.com/school4082434/cs331e-idb/-/issues">
                        GitLab issue tracker
                    </a>
                </li>
                <li>
                    <a href="https://gitlab.com/school4082434/cs331e-idb">
                        GitLab repository
                    </a>
                </li>
                <li>
                    <a href="https://gitlab.com/school4082434/cs331e-idb/-/wikis/Technical-Report-Phase-3">
                        GitLab wiki
                    </a>
                </li>
                <li>
                    <a href="https://docs.google.com/presentation/d/1EZvdkXDSZgO-OSK13z7swMH09u-j6e2faZNQH6K7TR4/edit?usp=sharing">
                        Presentation slides
                    </a>
                </li>
            </ul>

            <h1>Unit Tests</h1>
            <img src={UnitTests} style={{ marginLeft: '10px' }} alt="Unit tests"/>
            <br/>

            <h1>UML Diagram</h1>
            <img src={UML} width="400" alt="UML diagram" style={{ marginLeft: '10px' }}/>
            <br/>

            <h1> Tools Used </h1>
            <ul>
                <li>React</li>
                <li>HTML</li>
                <li>CSS</li>
                <li>Postgres</li>
                <li>SQLAlchemy</li>
                <li>GCP App Engine</li>
                <li>Flask</li>
                <li>asyncio/aiohttp</li>
                <li>make</li>
                <li>unittest</li>
                <li>Postman</li>
            </ul>

            <h1>Data</h1>
            <ul className="committeeHeader">
                <a href="https://api.congress.gov/#/">
                    <li className="committeeItem">
                        We used Library of Congress' congress.gov API for the majority of our data.
                    </li>
                <a href="https://theunitedstates.io/">
                    <li className="committeeItem">
                        Public domain images of members and committee membership rosters for the 117th Congress were obtained from the @unitedstates project.
                    </li>
                </a>
                </a>
            </ul>
 

        </Box>
    )
}

export default About