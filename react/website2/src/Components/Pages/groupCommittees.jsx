import { Link } from "react-router-dom";
import Navbar from "../NavBar/Navbar";
import TaskTable from "../Table/TaskTable";
import { Box } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import Utils from "../Utils";
import SearchBar from "../Search/SearchBar";
//* Column Definitions
//* Data and Column Definitions

const columns = [
    {
      accessorKey:"name",
      header:'Committee Name',
      size:250,
      cell: (props)=> <p>{props.getValue()}</p>
    },
  
    {
      accessorKey:'chamber',
      header:'Chamber',
      size: 500,
      cell: (props)=> <p>{props.getValue()}</p> 
    },
  
    {
      accessorKey:'n_sub',
      header:'Number of Subcommittees',
      size: 200,
      cell: (props)=> <p>{props.getValue()}</p> 
    },

    {
      header: 'Learn More',
      accessorFn: (row) => `${row.committee_id}`,
      cell: (props) => <Link to={`/committees/${props.getValue()}`}><p>Click here</p></Link>
    }
]

const GroupCommittees = () => {
    const [data, setData] = useState([]);
    const [allData, setAllData] = useState([])

    useEffect(() => {
        Utils.GetJSON("/committees/").then(resolvedData => {
            // Removes root -> [[0, [Array]], [1, [Array]]...]
            const value = Object.keys(resolvedData)
            const arrData = Object.entries(resolvedData[value])
            // Gets only second element of array
            const secondElements = arrData.map(innerArray => innerArray[1])
            setData(secondElements);
            setAllData(secondElements)
        });
    }, []);

    return(
        <Box>
            <Navbar/>
            <br/>
            <SearchBar data={data} setResults={setData} allData={allData}/>
            <br/>
            <TaskTable data={data} columns={columns}/>
        </Box>
    )
}

export default GroupCommittees