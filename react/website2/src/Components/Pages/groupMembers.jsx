import React, { useState, useEffect } from 'react';
import { Box, Button, ButtonGroup } from "@chakra-ui/react";
import Navbar from "../NavBar/Navbar";
import Card from "../Card/Card";
import Utils from "../Utils";
import SearchBar from '../Search/SearchBar';
import SearchResultsList from '../Search/SearchResultsList';

const GroupMembers = () => {
    // Pagination and data fetching
    const [allData, setAllData] = useState([]);
    const [data, setData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage] = useState(12);
    const [dataLength, setLength] = useState(10)

    // Search and filtering
    const [result, setResults] = useState([])

    useEffect(() => {
        Utils.GetJSON("/members/").then(resolvedData => {
            // Data from JSON is a nested JSON with a root header so 
            // we have to get rid of the root
            const value = Object.keys(resolvedData)
            const arrData = Object.entries(resolvedData[value])
            const secondElements = arrData.map(innerArray => innerArray[1])
            // For debugging
            //console.log(secondElements);
            setLength(secondElements.length)
            setAllData(secondElements); // Store all the processed data
            setData(sliceData(secondElements, 1, itemsPerPage)); // Set initial page data
        });
    }, []);

    // Slicing Data for pagination components
    const sliceData = (data, page, limit) => {
        const startIndex = (page - 1) * limit;
        const endIndex = startIndex + limit;
        return data.slice(startIndex, endIndex);
    };

    // Pagination
    const nextPage = () => {
        const newPage = currentPage + 1;
        setData(sliceData(allData, newPage, itemsPerPage));
        setCurrentPage(newPage);
    };

    const prevPage = () => {
        if (currentPage > 1) {
            const newPage = currentPage - 1;
            setData(sliceData(allData, newPage, itemsPerPage));
            setCurrentPage(newPage);
        }
    };

    // Sorting
    // If you're looking at the code, please stop. This is a horror
    const sortByState = () => {
        const sortedData = [...allData].sort((a, b) => {
            const stateA = (a.state || "").toUpperCase(); // ignore upper and lowercase
            const stateB = (b.state || "").toUpperCase(); // ignore upper and lowercase
            if (stateA < stateB) {
                return -1;
            }
            if (stateA > stateB) {
                return 1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const sortByStateR = () => {
        const sortedData = [...allData].sort((a, b) => {
            const stateA = (a.state || "").toUpperCase(); // ignore upper and lowercase
            const stateB = (b.state || "").toUpperCase(); // ignore upper and lowercase
            if (stateA < stateB) {
                return 1;
            }
            if (stateA > stateB) {
                return -1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const sortByPartyD = () => {
        const sortedData = [...allData].sort((a, b) => {
            const partyA = (a.party || "").toUpperCase(); // ignore upper and lowercase
            const partyB = (b.party || "").toUpperCase(); // ignore upper and lowercase
            if (partyA < partyB) {
                return -1;
            }
            if (partyA > partyB) {
                return 1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const sortByPartyR = () => {
        const sortedData = [...allData].sort((a, b) => {
            const partyA = (a.party || "").toUpperCase(); // ignore upper and lowercase
            const partyB = (b.party || "").toUpperCase(); // ignore upper and lowercase
            if (partyA < partyB) {
                return 1;
            }
            if (partyA > partyB) {
                return -1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const sortBySenate = () => {
        const sortedData = [...allData].sort((a, b) => {
            const chamberA = (a.chamber || "").toUpperCase(); // ignore upper and lowercase
            const chamberB = (b.chamber || "").toUpperCase(); // ignore upper and lowercase
            if (chamberA < chamberB) {
                return 1;
            }
            if (chamberA > chamberB) {
                return -1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const sortByHOR = () => {
        const sortedData = [...allData].sort((a, b) => {
            const chamberA = (a.chamber || "").toUpperCase(); // ignore upper and lowercase
            const chamberB = (b.chamber || "").toUpperCase(); // ignore upper and lowercase
            if (chamberA < chamberB) {
                return -1;
            }
            if (chamberA > chamberB) {
                return 1;
            }
            return 0; // names equal
        });

        setData(sliceData(sortedData, currentPage, itemsPerPage));
    };

    const clearSorting = () => {
        setData(allData, currentPage, itemsPerPage)
    }

    return (
        <Box>
            <Navbar/>
            <br/>
            <SearchBar data={allData} setResults={setResults} allData={allData}/>
            <SearchResultsList results={result}/>
            <p style={{ marginLeft: '13px' }} padding='10px'>Sort by:</p>
            <Button onClick={sortByState} style={{ marginLeft: '13px' }} padding='4px'>State ⏫</Button>
            <Button onClick={sortByStateR} style={{ marginLeft: '13px' }} padding='4px'>State ⏬</Button>
            
            <Button onClick={sortByPartyR} style={{ marginLeft: '13px' }} padding='4px'>Republican</Button>
            <Button onClick={sortByPartyD} style={{ marginLeft: '13px' }} padding='4px'>Democrat</Button>

            <Button onClick={sortBySenate} style={{ marginLeft: '13px' }} padding='4px'>Senate</Button>
            <Button onClick={sortByHOR} style={{ marginLeft: '13px' }} padding='4px'>House</Button>
            
            <Button onClick={clearSorting} style={{ marginLeft: '13px' }} padding='4px'>Clear Sorting</Button>
            <br/>
            {data.map((item, index) => (
                <Card key={index} props={item} />
            ))}
            <br/>
            <ButtonGroup size='sm' isAttached variant={'outline'} style={{ marginLeft: '13px' }} padding='4px'>
                <Button onClick={prevPage} isDisabled={currentPage === 1}> {"<"} </Button>
                <Button> {currentPage} </Button>
                <Button onClick={nextPage} isDisabled={currentPage*itemsPerPage >= dataLength} > {">"} </Button>
            </ButtonGroup>
        </Box>
    );
};

export default GroupMembers;
