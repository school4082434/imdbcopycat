import { Box } from "@chakra-ui/react"

const TestPage = () =>{
    return(
        <Box>
            <p>Hello World!</p>
        </Box>
    )
}

export default TestPage