import Navbar from "../NavBar/Navbar";
import MemberDetails from "../IndividualPages/Members/MemberDetails"
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const Member = () => {
  const [data, setData] = useState([])
  const { member_id } = useParams()

  useEffect(() => {
    Utils.GetJSON(`/members/${member_id}`).then(memberData => {
        console.log(memberData)
        setData(memberData); // Set the member data in state as an array with a single element
    })
  }, [member_id])

  if (!data || data.length === 0) {
    return <div>Loading...</div>; // or any other placeholder
  }
  
  return (
      <div>
        <Navbar/>
        <MemberDetails props={data} />
      </div>
  )
}

export default Member;
  
/*
          <Navbar/>
          <MemberDetails props={data} />
*/