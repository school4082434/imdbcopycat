import { Box } from "@chakra-ui/react";
import Navbar from "../NavBar/Navbar";
import TaskTable from "../Table/TaskTable";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Utils from "../Utils";
import SearchBar from "../Search/SearchBar";

//* Data and Column Definitions
// Column names (accessor key)
const columns = [
    {
		accessorKey:'bill_title',
		header:'Title',
		size:350,
		
		cell: (props)=><p>{props.getValue()}</p>
		//enableColumnFilter: true,
		//filterFn: "includeString"
    },
  
    {
      accessorKey:'bill_id',
      header:'Bill ID',
      size: 200,
      cell: (props)=> <Link to={`/bills/${props.getValue()}`}><p>{props.getValue()}</p> </Link>
    },
  
    {
      accessorKey:'bill_type',
      header:'Type of Bill',
      size: 200,
      cell: (props)=> <p>{props.getValue()}</p> 
    },

    {
        accessorKey:'bill_number',
        header:'Bill Number',
        size: 200,
        cell: (props)=> <p>{props.getValue()}</p> 
	},
]

// Page Building
const GroupBill = () => {
  const [data, setData] = useState([]);
  const [allData, setAllData] = useState([])

  useEffect(() => {
      Utils.GetJSON("/bills/").then(resolvedData => {
          // Removes root -> [[0, [Array]], [1, [Array]]...]
          const value = Object.keys(resolvedData)
          const arrData = Object.entries(resolvedData[value])
          // Gets only second element of array
          const secondElements = arrData.map(innerArray => innerArray[1])
          // Setting data
          setData(secondElements);
          setAllData(secondElements)
      });
  }, []);
    console.log(data)

    return(
        <Box>
            <Navbar/>
            <br/>
            <SearchBar data={data} setResults={setData} allData={allData}/>
            <br/>
            <TaskTable data={data} columns={columns}/>
        </Box>
    )
}

export default GroupBill