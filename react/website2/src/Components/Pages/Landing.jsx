import { Box } from "@chakra-ui/react"
import styles from "../../theme/landing.js"

import Navbar from "../NavBar/Navbar.jsx";
import { Link } from "react-router-dom";

const Landing = () => {

    return(
        <Box style={styles[".background"]}>
            <Navbar/>
            <div style={styles[".container"]}>
                <Link to={"/members/"}>
                    <button style={styles.buttonStyle}>
                    Get Started Here
                    </button>
                </Link>
            </div>
        </Box>
    )
}

export default Landing