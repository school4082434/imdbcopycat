import axios from "axios";

async function GetJSON(url) {
    const combinedUrl = "http://127.0.0.1:5000" + url;
    try {
        const response = await axios.get(combinedUrl);
        return response.data; // Returns data
    } catch (error) {
        console.error("Error fetching data:", error);
        return []; // Or appropriate error handling
    }
}

const Utils = { GetJSON }; // Assign the object to a variable

export default Utils; // Export the variable
