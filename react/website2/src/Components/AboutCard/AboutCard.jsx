import { Box } from "@chakra-ui/react"
import "../AboutCard/AboutCard.css"
//import "./Card.css"
import Daniel from "../../assets/Headshots/Daniel.jpg"
import Rahil from "../../assets/Headshots/Rahil.JPG"
import Jeffrey from "../../assets/Headshots/Jeffrey.jpg"
import Serene from "../../assets/Headshots/Serene.jpg"

const AboutCard = ({name, commits}) => {
    const img_src = {"Daniel":Daniel, "Rahil":Rahil, "Jeffrey":Jeffrey, "Serene":Serene}
    const bios = {"Daniel": "Daniel is a senior government major interested in technology policy.",
                  "Jeffrey": "Jeffrey is a senior on the pre-medical track with an emphasis on ML for biological applications.",
                  "Rahil": "Rahil is a senior Economics major with a minor in Finance.",
                  "Serene":"Serene is a junior informatics major focusing on human-centered data science."
                }
    const roles = {"Daniel": "APIs, database, backend routing, unit tests, Namecheap setup, GCP deployment",
                   "Jeffrey": "landing/home page, navigation bar, group pages, about page, routing",
                   "Rahil": "individual instance pages, group leader, technical reports",
                   "Serene": "database, models, technical reports, UML diagram"}
    
    const unitTests = {"Daniel": 9, "Serene":0, "Jeffrey":0, "Rahil":0}

    const issues = {"Daniel": 10, "Serene":0, "Jeffrey":12, "Rahil":0}

    return (
        <Box className="aboutCard">
            <img className="image" alt="name" src={img_src[name]}/>
            <br/>
            <p>{bios[name]}</p>
            <br/>
            <p>Roles: {roles[name]}</p>
            <br/>
            <p>Commits: {commits}</p>
            <p>Unit Tests: {unitTests[name]}</p>
            <p>Issues: {issues[name]}</p>
        </Box>
    )
}

export default AboutCard