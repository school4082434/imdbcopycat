import React from 'react'
import './Navbar.css'
import { Link } from 'react-router-dom'
import { Box } from "@chakra-ui/react";
import Flag from "../../assets/flag.png"

const Navbar = () => {

    return (
        <div className='navbar'>
            <img src={Flag} alt="Flag" width="60"/>
            <Link to={"/"}><h1>Hill Explorer</h1></Link>
            
            {/* Columns */}
            <ul>
                <li>
                    <Link to={"/"}>Home</Link>
                </li>
                <li>
                    <Link to={"/members/"}>Members</Link>
                </li>
                <li>
                    <Link to={"/bills/"}>Bills</Link>
                </li>
                <li>
                    <Link to={"/committees/"}>Committees</Link>
                </li>
                <li>
                    <Link to={"/about/"}>About Us</Link>
                </li>
            </ul>
        </div>
    )
}

export default Navbar

/* Search Bar
<div className='search-box'>
    <input type='text' placeholder='Search'/>
    <img src={search_icon_dark}/>
    {/* Icon for light and dark mode*/