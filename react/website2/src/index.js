import React from 'react'
import ReactDOM from 'react-dom/client'
import { ChakraProvider } from "@chakra-ui/react";
import theme from "./theme/theme.js";
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Landing from './Components/Pages/Landing.jsx';
import GroupMembers from './Components/Pages/groupMembers.jsx';
import GroupBill from './Components/Pages/groupBills.jsx';
import GroupCommittees from './Components/Pages/groupCommittees.jsx'
import About from './Components/Pages/About.jsx'
import Member from './Components/Pages/members.jsx';
import Bill from './Components/Pages/bills.jsx';
import Committee from './Components/Pages/committees.jsx'

const router = createBrowserRouter([
  {path: "/", element: <Landing/>}, // To replace with Landing
  {path: "members/", element: <GroupMembers/>},
  {path: "members/:member_id", element: <Member/>},
  {path: "bills/", element: <GroupBill/>},
  {path: "bills/:bill_id", element:<Bill/>},
  {path: "committees/", element: <GroupCommittees/>},
  {path: "committees/:committee_id", element:<Committee/>},
  {path: "about/", element: <About/>}
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <RouterProvider router={router}/>
    </ChakraProvider>
  </React.StrictMode>,
)