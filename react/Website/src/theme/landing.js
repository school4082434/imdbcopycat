const styles = {
    ".background":{
        backgroundImage: 'url("https://www.brookings.edu/wp-content/uploads/2016/06/congress001_original.jpg")',
        height: "100vh",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
    },
    ".container":{
        display: "flex",
        justifyContent: "center", // Centers children horizontally in the container
        alignItems: "center", // Centers children vertically if you want vertical centering as well
        height: "100vh",
    },
    buttonStyle: {
        padding: '10px 20px',
        fontSize: '16px',
        color: '#fff',
        backgroundColor: '#808080',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
    },
  };
  
export default styles