const STATUS_ON_DECK = { id: 1, name: "On Deck", color: "blue.300" };
const STATUS_IN_PROGRESS = {
  id: 2,
  name: "In Progress",
  color: "yellow.400",
};
const STATUS_TESTING = { id: 3, name: "Testing", color: "pink.300" };
const STATUS_DEPLOYED = { id: 4, name: "Deployed", color: "green.300" };
export const STATUSES = [
  STATUS_ON_DECK,
  STATUS_IN_PROGRESS,
  STATUS_TESTING,
  STATUS_DEPLOYED,
];

const DATA = [
  {
    chair: "Add a New Feature",
    status: STATUS_ON_DECK,
    due: new Date("2023/10/15"),
    notes: "This is a note",
  },
  {
    chair: "Write Integration Tests",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Use Jest",
  },
  {
    chair: "Add Instagram Integration",
    status: STATUS_DEPLOYED,
    due: null,
    notes: "",
  },
  {
    chair: "Cleanup Database",
    status: null,
    due: new Date("2023/02/15"),
    notes: "Remove old data",
  },
  {
    chair: "Refactor API Endpoints",
    status: STATUS_TESTING,
    due: null,
    notes: "",
  },
  {
    chair: "Add Documentation to API",
    status: null,
    due: new Date("2023/09/12"),
    notes: "Add JS Docs to all endpoints",
  },
  {
    chair: "Update NPM Packages",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Upgrade React & Chakra UI",
  },
  {
    chair: "Optimize Database Queries",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Optimize slow queries.",
  },
  {
    chair: "Implement User Authentication",
    status: STATUS_ON_DECK,
    due: new Date("2023/11/08"),
    notes: "OAuth2 and JWT auth.",
  },
  {
    chair: "Design User Interface Mockups",
    status: null,
    due: new Date("2023/09/30"),
    notes: "Create UI mockups.",
  },
  {
    chair: "Fix Cross-Browser Compatibility Issues",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Resolve browser issues.",
  },
  {
    chair: "Perform Security Audit",
    status: null,
    due: new Date("2023/10/22"),
    notes: "Security audit.",
  },
  {
    chair: "Create User Onboarding Tutorial",
    status: STATUS_ON_DECK,
    due: new Date("2023/11/15"),
    notes: "User onboarding guide.",
  },
  {
    chair: "Optimize Frontend Performance",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Improve performance.",
  },
  {
    chair: "Conduct Code Review",
    status: null,
    due: new Date("2023/10/05"),
    notes: "Code review meeting.",
  },
  {
    chair: "Implement Continuous Integration",
    status: STATUS_ON_DECK,
    due: new Date("2023/11/01"),
    notes: "Set up CI/CD pipelines.",
  },
  {
    chair: "Migrate to Cloud Hosting",
    status: STATUS_DEPLOYED,
    due: null,
    notes: "Cloud migration.",
  },
  {
    chair: "Create User Feedback Survey",
    status: null,
    due: new Date("2023/09/25"),
    notes: "User feedback survey.",
  },
  {
    chair: "Update User Documentation",
    status: STATUS_TESTING,
    due: null,
    notes: "Revise documentation.",
  },
  {
    chair: "Bug Fixing and QA Testing",
    status: null,
    due: new Date("2023/10/10"),
    notes: "Fix bugs and QA.",
  },
  {
    chair: "Implement Mobile App Support",
    status: STATUS_IN_PROGRESS,
    due: null,
    notes: "Add mobile support.",
  },
  {
    chair: "Refine User Permission System",
    status: null,
    due: new Date("2023/09/18"),
    notes: "Enhance permissions.",
  },
];

export default DATA;