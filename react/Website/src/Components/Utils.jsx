import axios from "axios"

// Assuming axios is already imported
async function GetJSON(url) {
    const combinedUrl = "http://127.0.0.1:5000" + url;
    try {
        const response = await axios.get(combinedUrl);
        return response.data; // Return the fetched data directly
    } catch (error) {
        console.error("Error fetching data:", error);
        return []; // Return an empty array or appropriate error handling
    }
}


export default {GetJSON}