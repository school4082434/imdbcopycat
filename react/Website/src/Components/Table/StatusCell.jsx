import { Box, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { STATUSES } from "../../data";

export const ColorIcon = ({color, ...props}) => (
    <Box w="12px" h="12px" bg={color} borderRadius="3px" {...props}/>
    )

const StatusCell = ({ getValue, row, column, table }) => {
    const {name, color} = getValue() || {}
  return (
        <Menu isLazy offset={[0,0]} flip={false} autoSelect={false}>
            <MenuButton h="100%" w="100%" textAlign="left" p={1.5} bg={color || 'transparent'} color={'gray.900'}>
                {name}
            </MenuButton>
        </Menu>
  )
}

export default StatusCell
