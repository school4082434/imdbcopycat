import { React, useState, useEffect } from 'react';
import './CommitteeDetails.css';
import { Link } from "react-router-dom";


const CommitteeDetails = ({ props }) => {
  // Destructure member object to access individual properties
  // const { name, membership, leadership, legislations, committee, chamber, type } = props;
  

  // Setting committees to update when props updates b/c async
  const [bills_assigned, setCommittees] = useState(props.bills_assigned || [])
  useEffect(() => {
    setCommittees(props.bills_assigned)
  }, [props]) // Re-run the effect if prop changes  
  //console.log(Object.keys(props))
  return (
    <div style={{ textAlign: 'center' }}>
      <h1>{props.name}</h1>
      <div>
        <p>
          <strong>Chamber:</strong> {props.chamber}
        </p>

        <p>
          <strong>Committee Type:</strong> {props.committee_type}
        </p>
        
      <div>
        <strong>Members:</strong> 
        <ul>
          {props.members && props.members.map((memberId, index) =>(
            <li key = {index}>
              <Link to={`/members/${memberId}`}>{memberId}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <strong>Bills Assigned:</strong> 
        <ul>
          {bills_assigned && bills_assigned.map((billId, index) =>(
            <li key={index}>
              <Link to={`/bills/${billId}`}>{billId}</Link>
            </li>
          ))}
        </ul>
      </div>

 
      </div>
    </div>
  );
};

export default CommitteeDetails;


