import React from 'react';
import './MemberDetails.css';
import { Link } from "react-router-dom"

const MemberDetails = ({props}) => {
  const img_src = "https://theunitedstates.io/images/congress/450x550/"+ props.member_id + ".jpg"
  return (
  <div style={{ textAlign: 'center' }}>
    <h1>{props.name}</h1>
    <br/>
    <img src={img_src} alt="Member" style={{ maxWidth: '400px', maxHeight: '400px', margin: '0 auto' }} />
    <div>
      <br/>
      <p>
        <strong>Party Name:</strong> {props.party}
      </p>
      
      <p>
        <strong>District:</strong> {props.district}
      </p>
      
      <p>
        <strong>State:</strong> {props.state}
      </p>

      <p>
        <strong>Chamber:</strong> {props.chamber}
      </p>
      
      <div>
        <strong>Committees:</strong> 
        <ul>
          {props.committees.map((committee, index) => (
            <li key={index}>
              <Link to={`/committees/${committee}`}>{committee}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <strong>Bills Sponsored:</strong> 
        <ul>
          {props.bills_sponsored.map((billId, index) => (
            <li key = {index}>
              <Link to={`/bills/${billId}`}>{billId}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <strong>Bills Cosponsored:</strong> 
        <ul>
          {props.bills_cosponsored.map((billId, index) =>(
            <li key={index}>
              <Link to={`/bills/${billId}`}>{billId}</Link>
            </li>
          ))}
        </ul>
      </div>

    </div>
  </div>
  );
};


// const MemberDetails = ({ props }) => {
//   // Destructure member object to access individual properties
//   const { photo_id, name, party, district, state, chamber, committee, leadership, bills_spon, bills_cospon } = props;

//   return (
//     <div style={{ textAlign: 'center' }}>
//       <h1>{name}</h1>
//       <br/>
//       <img src={photo_id} alt="Member" style={{ maxWidth: '400px', maxHeight: '400px', margin: '0 auto' }} />
//       <div>
//         <br/>
//         <p>
//           <strong>Party name:</strong> {party}
//         </p>
        
//         <p>
//           <strong>District:</strong> {district}
//         </p>
        
//         <p>
//           <strong>State:</strong> {state}
//         </p>

//         <p>
//           <strong>Chamber:</strong> {chamber}
//         </p>

//         <p>
//           <strong>Committee:</strong> {committee}
//         </p>

//         <p>
//           <strong>Leadership:</strong> {leadership}
//         </p>

//         <p>
//           <strong>Bills Sponsored:</strong> {bills_spon}
//         </p>

//         <p>
//           <strong>Bills Cosponsored:</strong> {bills_cospon}
//         </p>
 
//       </div>
//     </div>
//   );
// };

export default MemberDetails;


