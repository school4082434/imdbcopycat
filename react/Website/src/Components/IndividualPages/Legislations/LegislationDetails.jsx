import React, { useEffect, useState } from 'react';
import './LegislationDetails.css';
import { Link } from "react-router-dom";


const LegislationDetails = ( {props} ) => {
  // Destructure member object to access individual properties
  // const { title, bill_tnum, committee, passed, chamber, bill_subj, sponsors, cosponsors } = props;
  
  // Setting committees to update when props updates b/c async
  const [committees, setCommittees] = useState(props.committees || [])
  useEffect(() => {
    setCommittees(props.committees)
  }, [props]) // Re-run the effect if prop changes  

  // Setting sponsors to update when props updates b/c async
  const [sponsors, setSponsors] = useState(props.sponsors || [])
  useEffect(() => {
    setSponsors(props.sponsors)
  }, [props]) // Re-run the effect if props changes  

  // Setting cosponsors to update when props updates b/c async
  const [cosponsors, setCosponsors] = useState(props.cosponsors || [])
  useEffect(() => {
    setCosponsors(props.cosponsors)
  }, [props]) // Re-run the effect if props changes  

  return (
    <div style={{ textAlign: 'center' }}>
    <h1>{props.bill_title}</h1>
    <div>
      <p>
        <strong>Bill Type:</strong> {props.bill_type}
      </p>

      <p>
        <strong>Bill Number:</strong> {props.bill_number}
      </p>

      <p>
        <strong>Bill Subjects:</strong> {props.policy_area}
      </p>

      <div>
        <strong>Committees:</strong> 
        <ul>
          {committees && committees.map((committee, index) => (
            <li key={index}>
              <Link to={`/committees/${committee}`}>{committee}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <strong>Sponsors:</strong> 
        <ul>
          {sponsors && sponsors.map((sponsor, index) =>(
            <li key = {index}>
              <Link to={`/members/${sponsor}`}>{sponsor}</Link>
            </li>
          ))}
        </ul>
      </div>

      <div>
        <strong>Cosponsors:</strong> 
        <ul>
          {cosponsors && cosponsors.map((cosponsor, index) =>(
            <li key = {index}>
              <Link to={`/members/${cosponsor}`}>{cosponsor}</Link>
            </li>
          ))}
        </ul>
      </div>

    </div>
  </div>
  );
};

// const LegislationDetails = ({ props }) => {
//   // Destructure member object to access individual properties
//   const { title, bill_tnum, committee, passed, chamber, bill_subj, sponsors, cosponsors } = props;

//   return (
//     <div style={{ textAlign: 'center' }}>
//       <h1>{title}</h1>
//       <div>
//         <p>
//           <strong>Bill ID:</strong> {bill_tnum}
//         </p>

//         <p>
//           <strong>committee:</strong> {committee}
//         </p>
        
//         <p>
//           <strong>Status:</strong> {passed}
//         </p>

//         <p>
//           <strong>Chamber:</strong> {chamber}
//         </p>

//         <p>
//           <strong>Bill Subjects:</strong> {bill_subj}
//         </p>

//         <p>
//           <strong>Sponsors:</strong> {sponsors}
//         </p>

//         <p>
//           <strong>Cosponsors:</strong> {cosponsors}
//         </p>

//       </div>
//     </div>
//   );
// };


export default LegislationDetails;




