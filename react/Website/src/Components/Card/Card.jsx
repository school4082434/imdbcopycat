import { Button, Box} from "@chakra-ui/react"
import "./Card.css"
import { Link } from "react-router-dom"

// Props: {photo_id, name, party, state, chamber, memberid}
const Card = ({props}) => {
    const img_src = "https://theunitedstates.io/images/congress/450x550/"+ props.member_id + ".jpg"
    return (
        <Box className="card">
            <img className = "image" alt="name" src={img_src}/>
            <p>{props.name}</p>
            <p>{props.party}</p>
            <p>{props.state}</p>
            <p>{props.chamber}</p>
            <p>{props.memberid}</p>
            <br/>
            <Button><Link to={`/members/${props.member_id}`}>Show More</Link></Button>
        </Box>
    )
}

export default Card