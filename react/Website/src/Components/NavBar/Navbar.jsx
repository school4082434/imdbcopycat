import React from 'react'
import './Navbar.css'
import { Link } from 'react-router-dom'

{/*Importing logos*/}
import logo_dark from '../../assets/logo-white.png'

const Navbar = () => {

    return (
        <div className='navbar'>
            <h1>Hill Explorer</h1>
            {/* Columns */}
            <ul>
                <li>
                    <Link to={"/"}>Home</Link>
                </li>
                <li>
                    <Link to={"/members/"}>Members</Link>
                </li>
                <li>
                    <Link to={"/bills/"}>Bills</Link>
                </li>
                <li>
                    <Link to={"/committees/"}>Committees</Link>
                </li>
                <li>
                    <Link to={"/about/"}>About Us</Link>
                </li>
            </ul>
        </div>
    )
}

export default Navbar

/* Search Bar
<div className='search-box'>
    <input type='text' placeholder='Search'/>
    <img src={search_icon_dark}/>
    {/* Icon for light and dark mode*/