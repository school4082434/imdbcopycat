import { Box } from "@chakra-ui/react"
import "../AboutCard/AboutCard.css"
//import "./Card.css"
import Daryl from "../../assets/Plushies/Daryl.jpeg"
import Freddie from "../../assets/Plushies/Freddie.jpeg"
import Froodie from "../../assets/Plushies/Froodie.png"
import Toast from "../../assets/Plushies/Toast.jpeg"

const AboutCard = ({name, commits}) => {
    const img_src = {"Daniel":Froodie, "Jeffrey":Daryl, "Rahil":Toast, "Serene":Freddie}
    return (
        <Box className="aboutCard">
            <img className = "image" alt="name" src={img_src[name]}/>
            <br/>
            <p>{name} commits: {commits}</p>
            <br/>
        </Box>
    )
}

export default AboutCard