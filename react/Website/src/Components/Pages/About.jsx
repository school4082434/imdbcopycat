import { Box } from "@chakra-ui/react";
import Navbar from "../NavBar/Navbar";
import { useEffect, useState } from "react";
import Utils from "../Utils";
import AboutCard from "../AboutCard/AboutCard";
import UnitTests from "../../assets/unit-tests.png"

const About = () => {
    const [data, setData] = useState([])

    useEffect(() => {
        Utils.GetJSON("/about/").then(resolvedData => {
            setData(resolvedData);
        });
    }, []);
    console.log(data)
    return(
        <Box>
            <Navbar/>
            <br/>
            {Object.entries(data).map(([person, commits], index) =>(
                <AboutCard name={person} commits={commits}/>
            ))}
            <br/> <br/> 
            <h1>Unit Tests</h1>
            <img src={UnitTests}/>
        </Box>
    )
}

export default About