import Navbar from "../NavBar/Navbar";
import LegislationDetails from "../IndividualPages/Legislations/LegislationDetails"
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';


const Bill = () => {
  const [data, setData] = useState([]);
  const { bill_id } = useParams();

  useEffect(() => {
    // Fetch data for the specific bill using the bill_id
    Utils.GetJSON(`/bills/${bill_id}`).then(billData => {
        setData(billData); // Set the bill data in state
    });
  }, [bill_id]); // Add bill_id as a dependency to re-fetch if it changes

  return (
    <div>
      <Navbar />
      {/* Pass data as a prop directly */}
      <LegislationDetails props={data}/>
    </div>
  );
};


// const Bill = () => {
//     const [data, setData] = useState([]);
//     const { billType} = useParams();
//     const { billNumber} = useParams();
  
//     useEffect(() => {
//       Utils.GetJSON(`/members/${billType}/${billNumber}`).then(billData => {
//           setData([billData]); // Set the member data in state as an array with a single element
//       });
//     }, [billType, billNumber]);
  
//     return (
//         <div>
//             <Navbar/>
//             {data.map((item, index) => (
//                   <LegislationDetails key={index} props={item} />
//               ))}
//         </div>
//     );
//   };


// const Bill = (props) => {
//     // Example member data
//     const legislation = {
//       title: "H.R 240",
//       bill_tnum: "props.name",
//       committee: "Democratic Party",
//       passed: "District name",
//       chamber: "California",
//       bill_subj: "House",
//       sponsors: "committee",
//       cosponsors: "leadership",

//     };
  
//     return (
    
//       <div>
//         <Navbar/>
//         <LegislationDetails props={legislation} />
//       </div>
//     );
//   };
  
  export default Bill;
  
