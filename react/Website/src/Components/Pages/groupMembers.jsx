import { Box } from "@chakra-ui/react";
import Navbar from "../NavBar/Navbar";
import Card from "../Card/Card";
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';

//* Page Building

const GroupMembers = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        Utils.GetJSON("/members/").then(resolvedData => {
            // Assuming resolvedData is the structure you expect
            // Process it as needed, e.g., extract second elements
            // For example:
            // Removes root -> [[0, [Array]], [1, [Array]]...]
            const value = Object.keys(resolvedData)
            const arrData = Object.entries(resolvedData[value])
            // Gets only second element of array
            const secondElements = arrData.map(innerArray => innerArray[1])
            console.log(secondElements)
            setData(secondElements);
        });
    }, []);

    return (
        <Box>
            <Navbar/>
            <br/>
            {data.map((item, index) => (
                <Card key={index} props={item} />
            ))}
        </Box>
    );
};


export default GroupMembers
/*
{data.map((item, index) => (
    <Card key={index} props={item} />
))}

*/