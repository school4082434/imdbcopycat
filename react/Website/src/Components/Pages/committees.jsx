import Navbar from "../NavBar/Navbar";
import CommitteeDetails from "../IndividualPages/Committees/CommitteeDetails"
import { Box } from "@chakra-ui/react";
import Card from "../Card/Card";
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';


const Committee = () => {
  const [data, setData] = useState([]);
  const { committeeId } = useParams();

  useEffect(() => {
    Utils.GetJSON(`/committees/${committeeId}`).then(resolvedData => {
        setData(resolvedData);
      });
  }, []);
  
  return (
      <div>
          <Navbar/>
          <CommitteeDetails props={data} />
      </div>
  );
};

// const Committee = (props) => {
//     // Example member data
//     const member = {
//       name: "props.name",
//       membership: "Democratic Party",
//       leadership: "District name",
//       legislations: "California",
//       chamber: "House",
//       type: "committee",

//     };
  
//     return (
    
//       <div>
//         <Navbar/>
//         <CommitteeDetails props={member} />
//       </div>
//     );
//   };
  
  export default Committee;
  
