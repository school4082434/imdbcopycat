import Navbar from "../NavBar/Navbar";
import MemberDetails from "../IndividualPages/Members/MemberDetails"
import { Box } from "@chakra-ui/react";
import Card from "../Card/Card";
import Utils from "../Utils";
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const Member = () => {
  const [data, setData] = useState([]);
  const { memberId } = useParams();

  useEffect(() => {
    Utils.GetJSON(`/members/${memberId}`).then(memberData => {
        setData([memberData]); // Set the member data in state as an array with a single element
    });
  }, [memberId]);

  return (
      <div>
          <Navbar/>
          {data.map((item, index) => (
                <MemberDetails key={index} props={item} />
            ))}
      </div>
  );
};

 
// const Member = (props) => {
//     // Example member data
//     const member = {
//       photo_id: "https://www.congress.gov/img/member/l000174_200.jpg",
//       name: "props.name",
//       party: "props.party",
//       district: "props.district",
//       state: "props.state",
//       chamber: "props.chamber",
//       committee: "props.committee",
//       leadership: "props.leadership",
//       bills_spon: "props.spon",
//       bills_cospon: "props.cospon",

//     };
  
//     return (
    
//       <div>
//         <Navbar/>
//         <MemberDetails props={member} />
//       </div>
//     );
//   };
  
  export default Member;
  
